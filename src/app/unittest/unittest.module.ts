import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnittestRoutingModule } from './unittest-routing.module';
import { SampletestComponent } from './sampletest/sampletest.component';


@NgModule({
  declarations: [
    SampletestComponent
  ],
  imports: [
    CommonModule,
    UnittestRoutingModule
  ]
})
export class UnittestModule { }
