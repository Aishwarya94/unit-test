import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SampletestComponent } from './sampletest/sampletest.component';

const routes: Routes = [
  {path:'', component: SampletestComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnittestRoutingModule { }
