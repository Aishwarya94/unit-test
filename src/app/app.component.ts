import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  @Output() buttonClicked: EventEmitter<void> = new EventEmitter<void>();
  title = 'my-app';
  Count: number = 10;
  // num1: any;
  // num2: any;
  sum: any;
  CountNumber = 10;
  private Name: any;
  label:string = "Dotnet";
  studentName = "Name of the student";


IncreaseCount(num: number) {
  this.Count = this.Count + num;
}

DecreaseCount(num: number) {
  this.Count = this.Count - num;
}

ShowMessage(msg:string): string {
  return msg;
}

calculate(num1: any, num2: any) {
this.sum = num1 + num2;
return this.sum;
}

saveData() {

  this.calculate(5,5);

  }
  

  studentResult() {
    if(this.calculate(10,20) >= 40)
    {
      return "Pass";
    }
    else
    {
      return "Fail";
    }
  }
  
  // SaveDataIntoConsol(info) {
  // console.log(info);
  
  // }
  
  
  // SaveDetails(info) {
  // return info;
  // }
  StudentSchoolResult() {
    if (this.calculate(10, 20) >= 40) {
      var studentResult = "pass";
      return this.studentResult;
    }
    else 
    {
      var studentResult = "Fail";
      return this.studentResult;
    }
  }

  // studentResult(): "Pass" | "Fail" {
  //   if (this.calculate(10, 20) >= 40) {
  //     var studentResult = "pass";
  //     return this.studentResult;
  //   }
  //   else 
  //   {
  //     var studentResult = "Fail";
  //     return this.studentResult;
  //   }
  // }

  increaseNumber() {
    this.CountNumber = this.CountNumber + 1;
    }
    
    decreaseNumber() {
    this.CountNumber = this.CountNumber - 1;
    }

    private ShowName() {
      this.Name = "DotNet";
    }

    private recalculate(num1: any, num2: any) {
    this.sum = num1 + num2;
      return this.sum;
    }

    button1Click() {
      this.label = "Angular practice";
    }

    button2Click() {
      this.label = "label value change on button2";
    }

    onChangeInput() {
      this.label = "label value change";
    }

    onChangeLabelInput(event: any) {
       this.label = event.target.value;

    }

    setName() {
      this.studentName = "Dotnet Office";
    }


    buttonClick() {
      this.buttonClicked.emit();
      alert("Message saved successfully");
    }

  //  private recalculate(num1: any, num2: any) {
  //     this.sum = num1 + num2;
  //     return this.sum;
  //     }

}
