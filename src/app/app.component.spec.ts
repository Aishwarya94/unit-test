import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { Addition } from './Calculator';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

describe('AppComponent', () => {


let component: AppComponent;
let fixture: ComponentFixture<AppComponent>;
//the above statement gives the instance of html,css and ts file of app component
let deb : DebugElement;
// define in debugElemnt beforeeach

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    deb = fixture.debugElement;
  })

  afterEach( function() {
    console.log("After Each")
  })

  beforeAll(function() {
    console.log("before all");
  })

  afterAll(function() {
    console.log("after all");
  })

  it("test 1", () => {
    console.log("test 1");
    })
    
    it("test 2", () => {
    console.log("test 2");
    })
    
    it("test 3", () => {
    console.log("test 3");
    })
    
  

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'my-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('my-app');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('h1')?.textContent).toContain('Hello, my-app');
  });

  it('Show the addition result', () => {
   expect(Addition(10,20)).toBe(30);
  });

  xit('Show the addition result', () => {
    expect(Addition(10,20)).toBeLessThan(30);
   });

   it('ToBe and ToEqual Test case', () => {
   var a = "Hello";
   var b = "Hello";
   expect(a).toBe(b);
   });

   it('ToBe and ToEqual Test case type 2', () => {
    var a = ['1'];
    var b = ['1'];
    expect(a).toEqual(b);
    });

    it('ToBe and ToEqual Test Case', () => {
      var a = "Hello";
      expect(a).toBe("Hello");
    });

    it('ToBeTrue', () => {
        var a = true;
        expect(a).toBeTrue();

    });

    it('ToBe and ToEqual Test Case', () => {
      var a = true;
      expect(a).toBeTruthy();

    });

    it('ToBe greater than Test Case', () => {
      var a = 5;
      expect(a).toBeGreaterThan(4);
    });

    it('ToBe lesser than or equal to Test Case', () => {
      var a = 4;
      expect(a).toBeLessThanOrEqual(4);
    });

    it('Jasmine matcher - Match Function', () => {
      var input = "The dotnetoffice tutorials";
      var strPhone = "001-789-78-89"
      expect(input).toMatch(/dotnetoffice/);
      expect(input).toMatch(/dotnetoffice/i);
      expect(strPhone).toMatch(/\d{3}-\d{3}-\d{2}-\d{2}/);
    });

    it('Jasmin Matcher - toBeClodeTo', () => {
      var pi=3.1415926, e = 2.78;
      expect(pi).not.toBeCloseTo(e);
      expect(pi).toBeCloseTo(e,0);
      expect(4.334).toBeCloseTo(4.334);
      expect(4.334).toBeCloseTo(4.3345,1);
      });
  
  it('Jasmine matcher - toBeNull', function () {
    var nullValue = null;
    var valueUndefined;
    var notNull = "notNull";
    expect(null).toBeNull();
    expect(nullValue).toBeNull();
    expect(valueUndefined).not.toBeNull();
    expect(notNull).not.toBeNull();
  });

  it("Jasmine matcher - tobeNan", function() {
    expect(0/0).toBeNaN();
    expect(0/5).not.toBeNaN();
    });

it("Jasmine matcher - toBePositiveInfinity", function() {
  expect(1/0).toBePositiveInfinity();
});

it ("Increase count", () => {

  var comp = new AppComponent();

  comp.IncreaseCount(2);

  expect(comp.Count).toEqual(12);

  console.log("Increase");
});

it ("Decrease count", () => {

  var comp = new AppComponent();

  comp.DecreaseCount(2);

  expect(comp.Count).toEqual(8);
  console.log("Decrease");
});


it("Test - AAA pattern" , () => {
  //arrange
let comp = new AppComponent();

//act
let msg = comp.ShowMessage("Hello");

//Assert
expect(msg).toBe("Hello");
});


//Spy over the  method

it("SpyOn Method", () => {
  // spyon takes two parameters object(ie:- component and a method name)
  spyOn(component, 'calculate');
  component.saveData();
  expect(component.calculate).toHaveBeenCalled();
});

it("SpyOn Method - 1", () => {
  spyOn(component, 'calculate').and.returnValues(10, 20);
  // component.saveData();
  let result = component.studentResult();
  expect(result).toEqual("Pass");
}); 

it('Increase count click', () => {
  const h1 = deb.query(By.css('h1'));
  const btn = deb.query(By.css('#btnincreaseNumber'));
// for click the button
  btn.triggerEventHandler('click', {});
  // referesh the value
  fixture.detectChanges();
  expect(component.CountNumber).toEqual(parseInt(h1.nativeElement.innerText));
});

//calling a private method - important

// it('Calling a private method',() => {
//   component["ShowName"]();
//   expect(component["Name"]).toEqual("DotNet");
// });


it('Calling a private method',() => {
  let spyName = spyOn<any>(component, 'Showname');
  component["ShowName"]();
  expect(spyName).toHaveBeenCalled()
});

it('call private method - recalculate', () => {
  let spyMethod = spyOn<any>(component, 'recalculate');
  component["recalculate"](10, 20);
  expect(spyMethod).toHaveBeenCalled();
});


it('button 1', () => {
  const element : HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('#button1');
  expect(component.label).toEqual("Dotnet");

  element.click();
  fixture.detectChanges();
  expect(component.label).toEqual("Angular practice");
});


it('button 2', () => {
  const element : HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('#button1');
  expect(component.label).toEqual("Angular practice");

  element.click();
  fixture.detectChanges();
  expect(component.label).toEqual("label value change on button2");
});


it('textbox1', () => {
  const element : HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#textbox1');
  expect(component.label).toEqual("Dotnet");

  element.dispatchEvent(new Event('input'));
  fixture.detectChanges();
  expect(component.label).toEqual("label value change");
});


it('textbox2', () => {
  //get DOM element
  const element : HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#textbox2');
  expect(component.label).toEqual("Dotnet");
// take action according to the event attached to it
  element.dispatchEvent(new Event('input'));
  fixture.detectChanges();
  expect(component.label).toEqual("label value change");
});

// Two way data binding or asynchronous testing

it('set student name from component', (done) => {

  component.studentName = 'dotnetoffice_updated';
  fixture.detectChanges();
  // For synchronous call use whenstable
  fixture.whenStable().then(() => {
 // accessing the input element
 const element : HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#name');
 expect(element.value).toEqual('dotnetoffice_updated');
 done();
  });

  // Unit test case to check data changes on both input and interpolation
  // using done because done has no specification
 
  it('set textbox value', (done) => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      // getting the input element
      const element : HTMLInputElement = fixture.debugElement.nativeElement.querySelector('#name');
      //Updating text box value
      element.value = "textbox updated";
      // Use dispatch event when we write anything in textbox
      element.dispatchEvent(new Event('input'));
      //checking if the value is equal
      expect( element.value).toEqual(component.studentName);
      done();
    })
  });

  // once clicked the text box value should change

  it('set value unit test case', (done) => {
   fixture.detectChanges();
   const element : HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('#buttons');
   fixture.whenStable().then(() => {
    element.click();

    expect(component.studentName).toEqual("Dotnet Office");
    fixture.detectChanges();
    fixture.whenStable().then( () => {
      const element : HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('#buttons');
      expect(element.value).toEqual("DotNet Office");
      done();
    })
   });

  //  Event Binding

  it('Event Binding', () => {

    // spyOn the buttonClicked EventEmitter
    spyOn(component.buttonClicked, 'emit');

    //Find the button element
    const buttonElement = fixture.nativeElement.querySelector('#clickMe');
    
    //click the button
    buttonElement.click();

    //check if the event has been emitted
    expect(component.buttonClicked.emit).toHaveBeenCalled();
  });


  })

})


});


